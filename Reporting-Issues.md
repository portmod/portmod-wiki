*Note: Issues should report a problem with the software, not a problem you are encountering. They should be closed when the problem is fixed, not when a workaround has been found, since it is important that the issue stays visible so that others can find it if they run into the same problem, and to make sure that the underlying problem is fixed.*

[[_TOC_]]

## Where to open issues

Package-specific issues should be opened on the issue tracker for the package repository in question. To understand where an error is coming from, see the [Portmod's Structure](#portmods-structure) section below.
* The repository a package comes from can be found in the transaction list at the end of the package name in the form `::repo` when running in verbose mode, and will normally show up when using the search interface.
* If in doubt, open the issue on the package repository first. Issues can be moved later if it is determined there is an underlying problem with portmod, or a related issue can be opened.
* Do not open issues such as "installation of package X fails" on the main portmod issue tracker. Either report what in particular is wrong with portmod, or, if you don't know, open the issue on the package repository issue tracker.

Links to the package repository GitLab pages and their issue trackers can be found on the [wiki homepage](Home).

### Questions and Documentation

Generally, you should not create issues which are just questions about how do something. Check the documentation and the Wiki, and if you can't find it, open an issue about the lack of documentation, which will also let you find out the answer (see note at top; the issue is why you can't find out, not that you don't know).

Documentation issues with the main portmod documentation, found at https://portmod.readthedocs.io/en/stable/, can be opened on [the main issue tracker](https://gitlab.com/portmod/portmod/-/issues). This documentation is limited to documentation of the portmod package manager itself.

All other documentation can go on [the Wiki](https://gitlab.com/portmod/portmod/-/wikis/home), such as repository-specific setup information, packages, etc. 
Documentation issues for the wiki should be opened on [the Wiki's issue tracker](https://gitlab.com/portmod/portmod-wiki/-/issues).

# Portmod's Structure

Portmod is a collection of sofware, not a single monolithic program. Understanding how this software fits together is key to understanding the cause of problems and how they can be investigated and solved.

## Portmod's Components
Portmod Consists of four primary components: 

### The portmod tool itself
https://gitlab.com/portmod/portmod

This is the glue that holds everything together, but it does not actually install anything by itself, it just provides a common framework for package installation, a packaging runtime environment so that installation is reproduce-able, and handles package information for display to the user.

### Package Repositories
Package repositories are [Version Control Systems](https://en.wikipedia.org/wiki/Version_control_system) used to store portmod's profiles and packages (see next sections). Package repositories have a *remote* version, which is where changes to the repository are published, and *local* copies on user machines, which are what your portmod tool interacts with.

The primary package repositories are listed on [the Wiki's Home page](Home). The `portmod <prefix> select repo` tool can also be used to display and configure available package repositories on your local system.

You should regularly run the `portmod sync` command to update your local copies of the package repositories to match the remote ones. This is particularly important prior to reporting issues as your problem may have already been solved, so you should always do this before opening a new issue.

### Profiles
See https://portmod.readthedocs.io/en/stable/setup.html#select-a-profile. 

Primarily important when setting up a prefix, profiles let package developers set up shared configuration for packages such as where data is installed inside of a prefix, as well as default values for user configurable settings.

You can view important profile variables with the `portmod <prefix> info` command.

### Packages

A collection of metadata and scripts describing how to install something.

You can install all available updates by running `portmod <prefix> merge --update --deep @world`. `@world` is the [Set](https://portmod.readthedocs.io/en/stable/concepts/sets.html) containing all packages you've explicitly installed, as well as mandatory packages in the `@system` set. The `--update` flag skips packages which have not changed, and the `--deep` flag tells portmod to allow changes to all packages, not just the ones passed as arguments (in this case, all the dependencies of packages in `@world`).

## Package Installation
Packages run custom installation scripts when portmod installs them.
* Package scripts run in several phases, most of which are optional. All errors encountered in package scripts result in an error trace, followed by a message about a `SandboxedError`.
  * pre-installation via pkg_pretend and pkg_nofetch
  * src_unpack (`Unpacking package ...`)
  * src_prepare (`Preparing source in ...`)
  * src_install (`Installing <pkg> into <tmp>/image`)
  * pkg_postinst (runs after installation into prefix)
* Portmod handles the following during installation:
  * Fetching archives
  * Setting up the sandbox for package scripts
  * Checking the prefix for conflicts with the new files after src_install has run
  * Installing files from the `image` directory into the prefix
  * Removing the files from the old package (pkg_prerm being the one script that runs during package removal)
  * Adding metadata about the package to the vdb

## Other External Code
* The `cfg-update` command, which also runs automatically after package installation, runs modules such as [configtool](https://gitlab.com/portmod/configtool), which have their own issue trackers.
* Packages share common code which comes from `common/*` packages. This is also something entirely on the side of the package repository.