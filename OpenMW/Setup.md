*Note: If you're using WSL, you should also look at the [WSL Setup Guide](./WSL Setup).*

### Creating a prefix

You can create a prefix using the command `portmod init <prefix> <arch>`.

`<prefix>` is the name of the prefix you want to create. This name can be arbitrary, but it is recommended, unless you are intending on setting up multiple prefixes for the same architecture, to use the architecture as the prefix name (e.g. `openmw`).

`<arch>` is the architecture of the prefix (i.e. the game engine), for openmw this can be either `openmw` or `tes3mp`.

Unless you pass the `--no-confirm` argument to `portmod init`, it will prompt you to select package repositories and a profile.

There is a third, optional, argument: a directory, however it is not recommended to specify this for OpenMW prefixes as OpenMW mods are not supposed to be installed alongside the game engine files, and OpenMW's VFS is more capable of handling file conflicts than portmod is, so it would not be helpful to install on top of the Morrowind Data Files.

### Select a Profile

The profile provides default settings that are tailored for your game setup.

In addition to during prefix initialization, you can use `portmod <prefix> select profile list` to see a list of available profiles, and  `portmod <prefix> select profile set NUM` can be used to select the profile you want.

- `openmw/3.0`: A bare profile which does not consider `base/morrowind` to be a system package, allowing other `base` games to be installed in its place.
- `openmw/3.0/morrowind-tb-bm`: Indicates that you have the Morrowind data files with both expansion packs already installed on your system. `base/morrowind`, which links to the Morrowind data files, will be a system package, meaning it cannot be removed.

See [Profiles](./Profiles) for more information about the OpenMW profiles.

### Install System packages

Prior to installing anything, you may want to, if using a `morrowind` profile, set `MORROWIND_PATH` in [portmod.conf](Configuration/portmod-config) to point to your morrowind installation. If this variable is not set, the build file will try to automatically detect where morrowind is installed.

All the `openmw` profiles require certain "system" packages, which are necessary for the proper generation of configuration files and the proper functioning of mods after they have been installed.

Once prefix creation is complete you should install these system packages by performing a world update: `portmod <prefix> merge --update --deep @world` (or `portmod <prefix> merge -uD @world`).

Technically, `--deep` and `--update` are not necessary the first time, as they have no effect on packages which have not been installed, however it is a good idea to familiarize yourself with this command since it also is how you should install package updates.

## Where to go from here

Once you've set up a prefix, it's time to install packages and start playing OpenMW.

See [Basic Usage](https://portmod.gitlab.io/portmod/basic-usage.html) for a basic overview of the commands which can be used to install and update packages.

You can also browse [the openmw package repository](https://portmod.gitlab.io/openmw-mods/) to find packages to install.

If you're looking for a mod in particular, you can try searching for it using `portmod <prefix> search <name>`.

### Metapackages

As a starting point, you may want to look at available metapackages in the [meta-momw](https://portmod.gitlab.io/openmw-mods/meta-momw/) and [meta-misc](https://portmod.gitlab.io/openmw-mods/meta-misc/) categories. These packages represent collections of mods and usually provide mods which work well together.

Note that due to how the metapackages work, you do not have the choice of opting out of any packages in the metapackage (though you can always install additional packages). If you want only a subset of the packages in a metapackage, your only choice is to manually select the packages you want, and you will not receive updates automatically when the metapackage is updated.

## Migrating Manual Setups (Optional)

You can use the `portmod-migrate` tool to import archives you've already downloaded, as well as to detect packages which correspond to mods that you have installed manually. `portmod-migrate` can be installed using pip similarly to portmod.

https://gitlab.com/portmod/portmod-migrate

## Usage Notes

- Make sure the openmw-launcher is not running. If you install mods when the openmw-launcher is running the mods will be removed from the config file when the openmw-launcher is closed.
- When installing mods, relevant plugins are enabled automatically. There is no need to enable plugins using openmw-launcher.
