## Language specific settings

The L10N use-expand variable can be used to enable optional language support for mods (there are not very many of these at the moment).

This can be set in portmod.conf. E.g.:
```py
L10N = "fr"
```

## Compatibility

Via [ESMap](https://gitlab.com/bmwinger/esmap), plugin cell names, dialogue identifiers, and cell reference IDs which differ in the different official versions of the morrowind data files (making mods written for one version incompatible with the others), can be automatically fixed during installation. 

Language differences will obviously still be present, but this also works for mods which only change cell references and are affected by the issues with cell names and reference IDs.

The process is as follows:
1. set `MORROWIND_COMPAT` to the language of the version of morrowind you have installed (only "en", "fr", "de" and "ru" are supported). If not set, no attempt will be made to fix language identifier issues, and you will need to manually make sure that mods are compatible.
2. Install `bin/esmap`
3. When installing mods, esmap will be run on plugins if their packages have a different `MORROWIND_COMPAT`, or if one of the l10n flags is set. Any mods installed before step 1 is done may need to be re-installed.

### Notes:

Mappings only exist between English and other languages. In theory, we could map first to English, then the destination language, but in practice almost every mod in the repository includes an English version, so it's not worthwhile at the moment.

Mappings are only available from English to French, German and Russian, and vice versa. Note that the russian mapping is untested and is incomplete, but has been included for greater visibility in case anyone is interested in providing missing information such as the size and hashes of the Russian morrowind plugins (see [en-ru-mapping.yaml](https://gitlab.com/bmwinger/esmap/-/blob/master/mappings/en-ru-mapping.yaml)).

It would be possible to automatically detect the language version of Morrowind at install time and store it in a file for later reference (the esmap repo contains hashes and sizes of the different language versions). This could be done as an extension to help make this system more automatic, but for now it's probably best to keep things optional.