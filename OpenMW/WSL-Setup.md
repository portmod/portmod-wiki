*Note: Copied roughly from https://gitlab.com/portmod/portmod/-/issues/380#note_1220345011. TODO: Cleanup*

Follow [Setup](./Setup), but with the following changes

### Portmod Init Command

The command I used to init the prefix, need to pass in the location for the prefix to be on the windows filesystem. Specifically on the same drive as OpenMW for performance reasons and file path reasons.

`portmod init openmw openmw /mnt/d/OpenMWPortMod`

### Portmod Config Changes

These are needed for file path issues as well, portmod cant find stuff on the windows file system.

- `OPENMW_CONFIG_DIR="/mnt/c/Users/schra/OneDrive/Documents/My Games/OpenMW/"`
- `MORROWIND_PATH="/mnt/d/Steam/steamapps/common/Morrowind"`

*Note: There are defaults set in the profile now, but they are probably not reliable. You should double-check the paths by looking at the values in `portmod <prefix> info` and correcting like is mentioned above if necessary.*

### Set Environment Variables

This is more of a performance change for merging mods. I think it'll work just fine without these variables changes, but I believe keeping all the large file movements on the same filesystem will make things faster. I just ran these before install, obviously you could put them in the environment however you wish.

`export XDG_CACHE_HOME=/mnt/d/PortmodCache`

`export TMPDIR=/mnt/d/PortmodTmp`

### BSATool

It should be possible to [run windows executables inside WSL](https://learn.microsoft.com/en-us/windows/wsl/filesystems#run-windows-tools-from-linux), so the regular advice of adding it to your `PATH` in windows from the [OpenMW Page](./OpenMW) should work, however unlike in windows it won't be detected from the registry. If you have problems, try following the instructions below:

> I needed to get a copy of BSATool from the linux OpenMW and get it on my path.
> 
> I tried copying from windows to the linux filesystem, but I had issues. Maybe from extracting using windows 7zip? I have no > idea what it didnt work, but in the end extracting inside WSL and copying it worked.

```bash
tar -xf openmw-0.47.0-Linux-64Bit.tar.gz
cd openmw-0.47.0-Linux-64Bit
cp bsatool ~/.local/bin/
cp -r ./lib/ ~/.local/bin/
cp ./bsatool.x86_64 ~/.local/bin/
```