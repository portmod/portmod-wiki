# Profiles

See [the docs](https://portmod.readthedocs.io/en/latest/concepts/profiles.html) for an explanation of what profiles are, and the [Setup Guide](./Setup#select-a-profile) for how to use them.

## 3.0 (Current)

- `openmw/3.0`: A bare profile which does not consider `base/morrowind` to be a system package, allowing other `base` games to be installed in its place.
- `openmw/3.0/morrowind-tb-bm`: Indicates that you have the Morrowind data files with both expansion packs already installed on your system. `base/morrowind`, which links to the Morrowind data files, will be a system package, meaning it cannot be removed.

## 3.1 (Experimental)

- `openmw/3.1`
- `openmw/3.1/morrowind-tb-bm` (same as `openmw/3.1`, but based on `openmw/3.0/morrowind-tb-bm`)

3.0, but with Architecture versioning from openmw-mods!556.

This includes `modules/openmw-version` in the system set, which sets the [architecture version](https://portmod.readthedocs.io/en/stable/dev/arch_version.html#architecture-versioning) by reading OpenMW's version file when portmod starts up.

## Deprecated

### `default/openmw/2.0` (and friends)

2.0 profiles used the now deprecated `Pybuild1` class for packages.

Functionally the difference is that 2.0 used various builtin openmw-specific portmod features which have since been replaced with features implemented entirely in the repository and related tools like configtool (VFS, config sorting, etc.).

2.0 profiles and earlier also used to provide tribunal-only and bloodmoon-only profiles, which have since been dropped as it wasn't clear if they had ever been used. If you want to emulate them, use a "bare" profile like `openmw/3.0` and create a [profile.user](https://portmod.readthedocs.io/en/stable/config/profile.user.html) directory with the following files:

- `package.use.force`, containing `base/morrowind -tribunal bloodmoon` (or whatever combination) 
- `packages`, containng `base/morrowind`

## Removed

### 1.0/*
1.0 profiles were from before a major change to how portmod installs packages occurred prior to the first stable portmod release.