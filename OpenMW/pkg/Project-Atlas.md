- [Official Homepage and doc](https://www.nexusmods.com/morrowind/mods/45399)
- [Package Information](https://portmod.gitlab.io/openmw-mods/assets-misc/project-atlas/)

## Atlas Generators

If the `atlasgen` use flag is enabled, the `assets-misc/project-atlas` package will generate its own atlases using the provided atlas generation scripts.

This process relies heavily on [ImageMagick](./ImageMagick), which has [optional security policies](https://imagemagick.org/script/security-policy.php) restricting the maximum image sizes.

If you run into the error `convert: width or height exceeds limit`, then these sizes may need to be adjusted to get the atlases to generate when working with high resolution textures. The largest texture atlas in project-atlas 0.7 is 2048x16384 for 1024x1024 textures, and the widest is 4096x2048, so if you have the imagemagick policies set, you will need at least a 17KP max height and a 5KP max width. This would be doubled for 2048x2048 textures, etc.

E.g.

```xml
<policymap>
  ...
  <!-- Default Width of 8KP is sufficient -->
  <policy domain="resource" name="width" value="8KP"/>
  <!-- <policy domain="resource" name="height" value="8KP"/> -->
  <policy domain="resource" name="height" value="17KP"/>
  ...
</policymap>
```

Also note that project atlas gets rebuilt (to regenerate the texture atlases) when the textures visible in the VFS which it needs change.
This can include the first time you install it since it installs textures which aren't in the VFS until installation (honestly, should probably put those in a separate package...).

### Normal and Specular Maps

The atlas generators also support normal and specular atlas generation, via the `map_normal` and `map_specular` use expand flags, however these both require normal maps and specular maps for each of the textures used by the atlas generators.
These are not provided by `base/morrowind`, and there is currently no list of mods required to make this work (contributions welcome!). If you try to install Project Atlas without the required normal or specular maps installation will fail.

## Pre-generated Atlases

If you have `atlasgen` disabled, you probably want to enable the `atlas` [global use flag](https://portmod.gitlab.io/portmod/concepts/use-flags.html#global-flags). This flag is used by other packages to provide pre-generated texture atlases for project-atlas-compatible meshes.
