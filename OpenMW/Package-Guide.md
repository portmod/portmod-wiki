This guide is designed to be a very brief guide to how to write pybuild files. It will not cover every single aspect of Portmod and the pybuild system.

To introduce a simple pybuild, here is `land-flora/bloated-morrowind/bloated-morrowind-1.0.pybuild`.
```python
# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, InstallDir, File


class Package(MW):
    NAME = "Bloated Morrowind"
    DESC = "Adds around bloatspores to the wilderness of Morrowind"
    HOMEPAGE = "http://mw.modhistory.com/download-53-7817"
    LICENSE = "free-use"
    RDEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = "http://mw.modhistory.com/file.php?id=7817 -> bloatedmorrowind-1.0.zip"
    INSTALL_DIRS = [InstallDir("Data Files", PLUGINS=[File("Bloated Morrowind.esp")])]

```

## Basic Format
Note that a pybuild is basically just a python file. While any python code could be used, there is a special format necessary to work with the build system.

The file should start with a header comment as shown above. Following the header, any necessary imports should be included. For simple pybuilds such as this one, the `MW`, `InstallDir` and `File` imports should be all that are necessary.

## Variables
The most important part of the Pybuild is the class called `Package`, deriving `MW` (there are other superclasses available through the packages in the `common` directory in the repository). This Package class should define a number of variables. The most commonly used variables are briefly described below, and details on optional variables can be found in the documentation for [MW](https://gitlab.com/portmod/openmw-mods/-/blob/master/common/mw/mw-2.pybuild) (FIXME: missing proper documentation, but you can read the doc comments at the link) and its superclass [Pybuild2](https://portmod.readthedocs.io/en/stable/dev/pybuild/pybuild.html#pybuild.Pybuild2).

The `NAME` is a more nicely formatted version of the name included in the build file's filename.

The `DESC` is a short description of what the mod does.

The `HOMEPAGE` is a link to the mod's website or homepage (including the http(s):// prefix).

The `LICENSE` indicates how the mod may be used. It can be either a particular license (e.g. GPL-3), or one of several general licenses such as, in this case, `free-use`, which indicates that the author has not released the mod under a specific license, but has indicated somewhere that the mod can be freely used for any purpose.

`RDEPEND` lists runtime dependencies. That is, other packages that need to be installed for this mod to work properly. Note that there is also a package for Morrowind.

`KEYWORDS` lists architectures (either openmw or tes3mp) that are known to work with this mod. A ~ in front of the name indicates that it is unstable, or has not been tested thoroughly.

The `SRC_URI` tells portmod where to find the source files. Here, the URL does not contain a filename, so we use arrow notation (->) to indicate what Portmod will name the downloaded file.

`INSTALL_DIRS` tells portmod how the archive is structured. In this case, the relevant data directory is in a subdirectory inside the archive called `Data Files`. This subdirectory also contains a plugin called `Bloated Morrowind.esp`, which is listed under `InstallDir.PLUGINS` so that it is automatically added to the content section of openmw.cfg. Other files within `Data Files` are automatically included in the final installed result, however files in the archive that are not within an InstallDir will not be installed.

## Other Variables

`DATA_OVERRIDES` is used during sorting installation directories to ensure that the package is sorted after the given packages. The listed packages do not need to be dependencies of this package. `DATA_OVERRIDES` can also be specified for individual `InstallDir`s if the `PATCHDIR` option is used.

`TIER` is used during sorting to broadly prioritize mods (both data directories and plugins) in the load order.
- `TIER = 0` is used for base mods like `base/morrowind` to make sure they are first.
- `TIER = 1` is used for mods which directly replace things in base tier 0 mods, such as asset replacers.
- `TIER = 2` is for large mods and asset collections like Tamriel Rebuilt which should generally go earlier in the load order
- `TIER = a` is the default.
- `TIER = z` is for mods which should generally go after everything else.

## Generating Manifest

To generate the Manifest file that contains hashes of the source archives, use the `inquisitor manifest PATH_TO_FILE`. E.g. in the example above, `inquisitor manifest ./bloated-morrowind-1.0.pybuild`.

## Testing Pybuilds
The `inquisitor` command can also be used to check various attributes about a pybuild via the `scan` command (i.e. `inquisitor scan PATH_TO_FILE`). It will also scan the pybuild file if run from within the same directory (`inquisitor scan`) without passing the filename as an argument.

You should also try installing the package to make sure it works, which will also let you test mods in-game.

If you want to thoroughly test installation of a package with one or more use flags, you can use the `inquisitor test-install` command.

E.g. `inquisitor test-install openmw ./bloated-morrowind-1.0.pybuild`

## Further Details

See [Packaging Guidelines](OpenMW/Packaging-Guidelines) for recommendations about package creation.

Further details about the package format can be found [here](https://portmod.readthedocs.io/en/stable/dev/packages.html#package-development), in the packaging section of the portmod developer guide.
