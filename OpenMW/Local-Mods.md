You can integrate manually installed mods into configtool using configuration files as described in the configtool README: https://gitlab.com/portmod/configtool.

Furthermore, the `games-util/importmod-core` package in the `python` repository (upstream is part of [importmod](https://gitlab.com/portmod/importmod)) can generate these configuration files if you point it at a mod archive.

E.g. install with `portmod <prefix> merge importmod-core`

Run with `portmod <prefix> run importmod install <archive>`.

You will need to run configtool to update `openmw.cfg` with the command `portmod <prefix> module-update` (see [here](https://gitlab.com/portmod/portmod/-/wikis/OpenMW/OpenMW#configtool)).

If this is confusing, or you run into problems which can't be easily resolved (please also report the issues to either [importmod](https://gitlab.com/portmod/importmod/-/issues) or [configtool](https://gitlab.com/portmod/configtool/-/issues)), you may want to use the legacy method instead, as described in the section below.

# Legacy Method
*still supported, but will eventually be removed*

Portmod includes a certain amount of support for manually installed mods, which we call _local_ mods (following Linux conventions) since there is no information about them in the package repositories, and they are specific to the local machine.

## Adding Local Mods

To add a local mod, you should install it within a subdirectory of the `local` directory of your prefix `ROOT`.

This `local` subdirectory must follow the same naming rules as package names, as defined in [section 3.1.2 of the Package Manager Specification](https://projects.gentoo.org/pms/7/pms.html#x1-180003.1.2). Roughly speaking, this means it may only include ASCII letters, numbers, hyphens and underscores.

So that the local mods get added to configuration files such as `openmw.cfg`, you will need to run `portmod <prefix> cfg-update` after making changes to them.

## Special Files 
Special files will be detected automatically, depending on profile rules. 

For the `openmw` repository, all `esp`, `esm`, `omwaddon`, and `omwgame` files within the root of the directory will be enabled as plugins, as well as all `bsa` files added as archives to the VFS.

If files exist within the `local` directory which you do not want to be enabled, they must be deleted, moved or renamed so that they are no longer detected.

## Caveats

As local mods are have no package file to store metadata about them, it is up to the user to manually configure and maintain them.
Dependencies will not be enforced, sorting rules will need to be added as [User Sorting Rules](OpenMW/User-Sorting-Rules) and they will not receive updates of any kind.

When adding User Sorting Rules, you can refer to local packages using `local/{LOCAL_PKG}` as the identifying package atom.

Currently portmod does not support storing local mods outside of `$ROOT/local`, however you can make `$ROOT/local` be a symlink to the directory of your choice.