BSA files are used by Bethesda games to store game assets.

Portmod repositories use these to create a Virtual File System for several games, listed below. This page serves as documentation of the BSA support for those games to provide a common reference for anyone working with the packages. Users should not need to do anything mentioned on this page manually.

## Tools

Known CLI tools for creating BSA files include:
- BSArch (from [TES5Edit](https://github.com/TES5Edit/TES5Edit)): Build process is not documented.
- [bsatool2](https://gitlab.com/bmwinger/bsatool2): A fork of OpenMW's bsatool which supports archives from later games, incorporating code from BSArch.

GUI tools for creating BSA files include:
- [Archive](https://www.creationkit.com/index.php?title=Archive.exe)/[Archive2](https://www.creationkit.com/fallout4/index.php?title=Archive2): Archive tools included with the Creation Kit for their respective game. Archive is for creating `.bsa` files, and Archive2 is for creating `.ba2` files.


## Games

### The Elder Scrolls IV: Oblivion

The vanilla engine does not load BSA files in a configurable order, so BSA files cannot reliably override each other.

Proper BSA support requires the use of [SkyBSA](https://github.com/DavidJCobb/oblivion-SkyBSA), an OBSE plugin which makes BSA archives work the way they do in TES V: Skyrim. This package is included as the system package [dev-util/skybsa](https://portmod.gitlab.io/oblivion/dev-util/skybsa) as a consequence, so it will be installed by default.

### Fallout: New Vegas

By default, BSA files will not override BSA files provided by the base game. To do this, the [JIP LN NVSE Plugin](https://www.nexusmods.com/newvegas/mods/58277) is needed, and a empty text file must be created with the following name, `<Name of BSA>.override`. For example, if the BSA is called `example.bsa`, the text file must be called `example.override`.

Note that the following files/directories cannot be overridden, even with the above method:

* `sky/atmosphere.nif`
* `sky/clouds.nif`
* `characters/camerashake.nif`
* Everything under `characters/weaponwobbles/`

### Fallout 4

An ESP plugin (can be a blank one) is required to load archives. The archives must setup in the following way:
* Textures go in their own archive named `<ESP name> - Textures.ba2`. It must be compressed.
* Everything else goes in an archive named `<ESP name> - Main.ba2`. If this archive contains sounds, it can't be compressed. If it doesn't, it's fine to compress it.

##### Quirks
* The game won't load sounds in a compressed archive
* The game won't load archives named `<ESP name> - Sound.ba2` without an (unknown) INI tweak.  

## External documentation
* [Fallout NV](https://geckwiki.com/index.php?title=BSA_Files)
* [Skyrim](https://en.uesp.net/wiki/Skyrim_Mod:Archive_File_Format)
* [NexusMods Wiki](https://wiki.nexusmods.com/index.php/Bethesda_mod_archives)
