These games are grouped together since they are handled almost identically. They have not received very much attention, and need a bit of polish, so they should be considered **unstable**, however they do generally seem to work.

- **The Elder Scrolls IV: Oblivion (oblivion)**: https://gitlab.com/portmod/oblivion.git
- **Fallout: New Vegas (fallout-nv)**: https://gitlab.com/portmod/fallout-nv.git
- **Fallout 4 (fallout-4)**: https://gitlab.com/portmod/fallout-4.git

### Setup
These games use a BSA-based VFS which requires the installation of [bsatool2](https://gitlab.com/bmwinger/bsatool2). Pre-built binaries are now available for linux and will be installed as a dependency. On Windows and macOS bsatool2 must be compiled manually and placed in your `PATH` (until pre-built binaries are available on those platforms).

Since these games do not support separate directories for mods, the portmod prefix for the games must be installed on top of the game's data files (onto the directory containing the game executable). See the details about installing on top of an existing directory in the [Portmod Setup Guide](https://portmod.gitlab.io/portmod/setup.html#creating-a-prefix).

### Development

The common code for handling the BSA-based VFS is stored in the [bsa](https://gitlab.com/portmod/bsa) repository.

See [BSA](./BSA) for details about BSA files, the software to handle them, and their quirks.

One major missing feature is the ability to generate merged patches, however that can reasonably be done manually after installing mods. Wrye Bash has plans for a headless mode (see [here](https://github.com/wrye-bash/wrye-bash/issues/600)), which should make it possible to generate bashed patches automatically, though being able to also configure them via a file rather than the GUI would probably take a lot of work.

Note that the BSA-based VFS has certain limitations. See https://gitlab.com/portmod/configtool/-/issues/17